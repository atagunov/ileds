#ifndef __INC_TIMER_MINIM_H
#define __INC_TIMER_MINIM_H

class timerMinim
{
  private:
    uint32_t _timer = 0;
    uint32_t _interval = 0;

  public:
    timerMinim(uint32_t interval) {
      _interval = interval;
      _timer = millis();
    }

    void setInterval(uint32_t interval) {
      _interval = interval;
    }

    boolean isReady() {
      if ((long)millis() - _timer >= _interval) {
        _timer = millis();
        return true;
      } else {
        return false;
      }
    }

    void reset() {
      _timer = millis();
    }



};


#endif