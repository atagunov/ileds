#pragma once
#include <vector>
#include <fastled.h>
#include <algorithm>
#include "helper.h"

enum MatrixType 
{
    ZIGZAG_TYPE, 
    PARALLEL_TYPE
};

enum ConnectionAngle
{
    LEFT_BOTTOM,
    LEFT_TOP,
    RIGHT_TOP,
    RIGHT_BOTTOM
};

enum StripDirection
{
    TO_RIGHT,
    TO_UP,
    TO_LEFT,
    TO_DOWN
};


class ToRealConverter
{
    private:
        const int _width;
        const int _height;
        const ConnectionAngle _connection_angle;
        const StripDirection _strip_direction;
        const MatrixType _matrix_type;

        inline bool _is_swap()
        {
            return _strip_direction == TO_DOWN || _strip_direction == TO_UP;
        }

        inline std::pair<int, int> _swap(int x, int y)
        {
            if(_is_swap())
                return std::make_pair(y, x);
            else
                return std::make_pair(x, y);
        }

    public:
        ToRealConverter(int width, int height, 
            const ConnectionAngle connection_angle, const StripDirection strip_direction, const MatrixType matrix_type): 
            _width(width), _height(height), _connection_angle(connection_angle), 
            _strip_direction(strip_direction), _matrix_type(matrix_type)
        {
            // static_assert((connection_angle == LEFT_BOTTOM  && (strip_direction == TO_RIGHT || strip_direction == TO_UP)) || 
            //               (connection_angle == LEFT_TOP     && (strip_direction == TO_RIGHT || strip_direction == TO_DOWN)) || 
            //               (connection_angle == RIGHT_TOP    && (strip_direction == TO_LEFT || strip_direction == TO_DOWN)) || 
            //               (connection_angle == RIGHT_BOTTOM && (strip_direction == TO_LEFT || strip_direction == TO_UP)),
            //               "Not valid configuration"); 
        }

        inline int real_width()
        {
            if (_is_swap())
                return _height;
            else
                return _width;
        }

        inline std::pair<int, int> real_xy(int x, int y)
        {
            if (_connection_angle == LEFT_TOP || _connection_angle == RIGHT_TOP)
                y = _height - y - 1;

            if (_connection_angle == RIGHT_TOP || _connection_angle == RIGHT_BOTTOM)
                x = _width - x - 1;

            return _swap(x, y);
        }

        // получить номер пикселя в ленте по координатам
        virtual int getPixelIndex(int x, int y)
        {
            if (_connection_angle == LEFT_TOP || _connection_angle == RIGHT_TOP)
                y = _height - y - 1;

            if (_connection_angle == RIGHT_TOP || _connection_angle == RIGHT_BOTTOM)
                x = _width - x - 1;

            if (_is_swap())
                std::swap(x, y);

            int w = real_width();
            // if (matrix_type == PARALLEL_TYPE || y % 2 == 0)
            if (y % 2 == 0)
                // четная строка или параллельная матрица
                return (y * w + x);
             else 
                // матрица зигзаг и нечетная строка
                return (y * w + w - x - 1);
        }

};



class Screen: private NonCopyable
{
    private:
        int _width;
        int _height;
        std::vector<CRGB> _leds;
        const uint8_t _brightness;
        const uint8_t _pin;
        const uint8_t _volt_limit;
        const uint32_t _current_limit;
        ToRealConverter* _coordinate_converter_ptr;

    public:
        Screen(int width, int height, int pin, ToRealConverter* coordinate_converter_ptr,
               int brightness=32, uint8_t volt_limit=5, uint32_t current_limit=2000):
           _width(width), _height(height),  _brightness(brightness), _pin(pin),
           _volt_limit(volt_limit), _current_limit(current_limit),
           _coordinate_converter_ptr(coordinate_converter_ptr)
        {
            _leds.resize(width * height);
        };

        int getWidth()
        {
            return _width;
        }

        int getHeight()
        {
            return _height;
        }

        int ledCount()
        {
            return _width * _height;
        }

        CRGB* leds()
        {
            return &_leds[0];
        }


        void setup(CLEDController& controller)
        {
            pinMode(_pin, OUTPUT);
            FastLED.addLeds(&controller, leds(), ledCount()).setCorrection( TypicalLEDStrip );
            FastLED.setBrightness(_brightness);
            FastLED.setMaxPowerInVoltsAndMilliamps(_volt_limit, _current_limit);
            FastLED.clear();
        }

        // функция отрисовки точки по координатам X Y
        void drawPixelXY(int x, int y, const CRGB& color) 
        {
            if (x < 0 || x > _width - 1 || y < 0 || y > _height - 1)
                return;
            int thisPixel = _coordinate_converter_ptr->getPixelIndex(x, y);
            _leds[thisPixel] = color;
        }

        // функция получения цвета пикселя в матрице по его координатам
        const CRGB getColorXY(int x, int y)
        {
            int index = _coordinate_converter_ptr->getPixelIndex(x, y);
            return getColor(index);
        }

        // функция получения цвета пикселя по его номеру
        const CRGB getColor(int index)
        {
            if (index < 0 || index > _leds.size() - 1)
            {
                Serial.println("invalid!!!");
                return CRGB::Black;
            }
            return _leds[index];
        }


};


