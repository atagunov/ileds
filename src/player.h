#pragma once

#include <vector>
#include "effect.h"

class Player
{
    private:
        std::vector<BaseEffect*> _modes;
        int _current_mode;
        Screen* _screen_ptr;
    public:
        Player(Screen* screen_ptr):
            _current_mode(0), _screen_ptr(screen_ptr)
        {
            // _modes.push_back(new ScrollingEdgePoint(CRGB::Blue));
            // _modes.push_back(new ScrollingPoint(CRGB::Yellow, 60));
            _modes.push_back(new RunningString("Красный!", CRGB::Red));
            _modes.push_back(new RunningString("Blue", CRGB::Blue));
        }

        ~Player()
        {
            for(auto mode: _modes)
            {
                delete mode;
            }
        }

        BaseEffect* get_current_mode()
        {
            return _modes[_current_mode];
        }

        void process()
        {
            BaseEffect* mode = get_current_mode();
            mode->process(*_screen_ptr);
        }
};
