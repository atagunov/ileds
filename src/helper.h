#pragma once

class NonCopyable
{
  protected:
    NonCopyable() {}
    ~NonCopyable() {}
  private: 
    NonCopyable(const NonCopyable &);
    NonCopyable& operator=(const NonCopyable &);
};


int crgb_to_int(const CRGB color)
{
    return color.r << 16 | color.g << 8 | color.b;
}