#pragma once

#include <string>
#include "fastled.h"
#include "timerMinim.h"
#include "fonts.h"
#include "helper.h"

class BaseEffect: private NonCopyable
{
    private:
        std::string _name;
        timerMinim _timer;
        bool _setup_required;

    public:
        BaseEffect(const std::string& name, int step_interval):
           _timer(step_interval), _setup_required(true)
        {
            _name = name;

        };

        virtual ~BaseEffect() {};

        const std::string& getName() {return _name;}
        virtual void setup(Screen& screen)=0;
        virtual bool step(Screen& screen)=0;
        void process(Screen& screen)
        {
            if(_setup_required)
            {
                _setup_required = false;
                setup(screen);
            }

            if (_timer.isReady())  //  || (!BTcontrol && !gamemodeFlag)
                step(screen);
        }


};


class ScrollingPoint: public BaseEffect
{
    private:
        int _offset;
        const CRGB _color;
        CRGB _previous_color;
    public:
        ScrollingPoint(const CRGB& color, int speed=30):
            BaseEffect("scrolling_point", speed), _color(color)
        {

        }

        virtual void setup(Screen& screen)
        {
            _offset = 0;
            _previous_color = screen.leds()[_offset];

        }
        virtual bool step(Screen& screen)
        {
            CRGB* leds = screen.leds();
            leds[_offset] = _previous_color;
            _offset++;
            bool completed = false;
            if (_offset >= screen.ledCount())
                _offset = 0;
                completed = true;
            _previous_color = leds[_offset];
            leds[_offset] = _color;
            FastLED.show();
            return completed;
        }
};


class ScrollingEdgePoint: public BaseEffect
{
    private:
        int _x;
        int _y;
        const CRGB _color;
        CRGB _previous_color;
    public:
        ScrollingEdgePoint(const CRGB& color, int speed=560):
            BaseEffect("scrolling_point", speed), _color(color)
        {
        }

        virtual void setup(Screen& screen)
        {
            _x = 0;
            _y = 0;
            _previous_color = screen.getColorXY(_x, _y);
        }

        bool _next(int& edge_index, int& second, int width, int direction)
        {
            edge_index += direction;
            if (edge_index >= width)
            {
                edge_index = width - 1;
                if (second == 0)
                    second++;
                else
                    second--;
                return true;
            }

            if (edge_index < 0)
            {
                edge_index = 0;
                if (second == 0)
                    second++;
                else
                    second--;
                return true;
            }
            return false;
        }

        bool next(Screen& screen)
        {
            int width = screen.getWidth();
            int height = screen.getHeight(); 

            if (_y == 0)
            {
                _next(_x, _y, width, 1);
                return false;
            }

            if (_x == width - 1)
            {
                _next(_y, _x, height, 1);
                return false;
            }

            if (_y == height - 1)
            {
                _next(_x, _y, width, -1);
                return false;
            }

            return _next(_y, _x, height, -1);
        }


        virtual bool step(Screen& screen)
        {
            screen.drawPixelXY(_x, _y, _previous_color);

            Serial.print("previous");
            Serial.println(crgb_to_int(_previous_color), HEX);

            bool completed = next(screen);
            _previous_color = screen.getColorXY(_x, _y);

            screen.drawPixelXY(_x, _y, _color);
            FastLED.show();
            return completed;
        }
};


#define MIRR_V 0
#define MIRR_H 0

class RunningString: public BaseEffect
{
    private:
        const std::string _text;
        const CRGB _color;
        const CRGB _backColor;
        int _offset;
        int _vertical_position;
        int _let_width = 5;
        int _let_height = 8;
        int _let_space = 1;
        bool _horizontal;
    public:
        RunningString(const std::string& text, const CRGB& color, const CRGB& backColor=CRGB::Black,
                      const int vertical_position=-1, int speed=100, bool horizontal=true):
            BaseEffect("string", speed), _text(text), _color(color), _backColor(backColor),
             _vertical_position(vertical_position), _horizontal(horizontal)
        {
        }

        virtual void setup(Screen& screen)
        {
            _offset = screen.getWidth();
        }

        void drawLetter(byte letter, int offset, const CRGB& color, Screen& screen)
        {
            int width = screen.getWidth();
            int height = screen.getHeight();
            int start_pos = 0, finish_pos = _let_width;

            int let_height = _let_height;
            if (let_height > height) let_height = height;

            int offset_y;
            if (_vertical_position >= 0)
                offset_y = _vertical_position;
            else
                offset_y = (height - _let_height) / 2;     // по центру матрицы по высоте

            if (offset < -_let_width || offset > width)
                return;
            if (offset < 0)
                start_pos = -offset;
            if (offset > (width - _let_width))
                finish_pos = width - offset;

            for (byte i = start_pos; i < finish_pos; i++) 
            {
                int thisByte;
                if (MIRR_V) 
                    thisByte = getFont(letter, _let_width - 1 - i);
                else
                    thisByte = getFont(letter, i);

                for (byte j = 0; j < _let_height; j++)
                {
                    boolean thisBit;

                    if (MIRR_H)
                        thisBit = thisByte & (1 << j);
                    else
                        thisBit = thisByte & (1 << (let_height - 1 - j));

                    // рисуем столбец (i - горизонтальная позиция, j - вертикальная)
                    if (thisBit)
                        screen.drawPixelXY(offset + i, offset_y + j, color);
                }
            }
        }

        // интерпретатор кода символа в массиве fontHEX (для Arduino IDE 1.8.* и выше)
        uint8_t getFont(uint8_t font, uint8_t row) 
        {
            font = font - '0' + 16;   // перевод код символа из таблицы ASCII в номер согласно нумерации массива
            if (font <= 90) {
                return pgm_read_byte(&(fontHEX[font][row]));     // для английских букв и символов
            } else if (font >= 112 && font <= 159) {    // и пизд*ц ждя русских
                return pgm_read_byte(&(fontHEX[font - 17][row]));
            } else if (font >= 96 && font <= 111) {
                return pgm_read_byte(&(fontHEX[font + 47][row]));
            }
            return 0;
        }


        virtual bool step(Screen& screen)
        {
            FastLED.clear();

            int n = 0;
            for(auto it=_text.begin(); it != _text.end(); ++it)
            {
                byte c = static_cast<byte>(*it);
                if (c <= 191)
                // если русская буква, то пропускаем utf-8 префикс
                {
                    drawLetter(c, _offset + n * (_let_width + _let_space), _color, screen);
                    n++;
                }
            }
            bool completed = false;
            _offset--;
            if (_offset < -n * (_let_width + _let_space)) 
            {    // строка убежала
                _offset = screen.getWidth() + 3;
                completed = true;
            }
            FastLED.show();
            return completed;
        }

};
